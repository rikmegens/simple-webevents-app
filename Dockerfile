# Use the Python3.7.2 image
FROM python:3.7.2-stretch

#Maintainer
MAINTAINER Rik Megens (rmegens@redhat.com)

# Set the working directory to /app
WORKDIR	/webapp

# Copy the current directory contents into the container at /app
ADD . /webapp

# Install the dependencies
RUN pip install -r requirements.txt

# run the command to start uWSGI
CMD ["waitress-serve", "--call", "runapp:create_app"]